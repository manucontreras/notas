# OSINT para periodistas

* Autor: Manu Contreras ([@mcontreras](https://mobile.twitter.com/mcontreras))
* Versión: 07-01-2018

[OSINT](https://en.wikipedia.org/wiki/Open-source_intelligence) (Open-source intelligence) de software y servicios para periodistas, bloggers e investigadores.

## Buscadores

### Generales

* [Google](https://www.google.com/)
* [DuckDuckGo](https://duckduckgo.com/)

### Imágenes

* [Yandex Images](https://yandex.ru/images/)
* [Google Images](https://images.google.com/)
* [Bing Images](https://www.bing.com/images/)
* [TinyEye](https://tineye.com/)

## Mapas

* [Baidu Maps](https://map.baidu.com)
* [Bing Maps](https://www.bing.com/maps/)
* [DualMaps](http://www.dualmaps.com/)
* [Geolocatethis](https://github.com/musafir-py/geolocatethis)
* [Google Earth](https://www.google.com/earth/)
* [Google Maps](https://maps.google.com/)
* [HERE WeGo](https://wego.here.com/)
* [OpenStreetMap](https://www.openstreetmap.org/)
* [SatellitesPro](https://satellites.pro )

## Navegadores

* [Brave](https://brave.com/)
* [Tor](https://www.torproject.org/)


## Redes Sociales

### Facebook

### Twitter

* [Archive Twitter](https://github.com/motherboardgithub/archive_tweet)
* [Twitter Delete and Unlike Bot](https://github.com/JacksonBates/tweet-delete-bot)

### Reddit